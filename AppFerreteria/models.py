
from django.db import models
from django.dispatch import receiver
from django.db.models.signals import post_delete
from django.conf import settings

class Cargo(models.Model):
    ide = models.IntegerField()
    nombre = models.CharField(max_length=200, blank=True, null=True)

    def __str__(self):
        return self.nombre


class Cliente(models.Model):
    ide = models.CharField( max_length=50)
    user = models.OneToOneField(settings.AUTH_USER_MODEL, on_delete=models.CASCADE, blank=True, null=True)
    tipo = models.ForeignKey( 'TipoIdentificacion', on_delete=models.CASCADE, blank=True, null=True)
    nombres = models.CharField(max_length=100, blank=True, null=True)
    apellidos = models.CharField(max_length=100, blank=True, null=True)
    contacto = models.CharField(max_length=100, blank=True, null=True)
    otro_telf = models.CharField(max_length=100, blank=True, null=True)
    email = models.CharField(max_length=100, blank=True, null=True)
    tipo_persona = models.ForeignKey( 'TipoPersona', on_delete=models.CASCADE, blank=True, null=True)
    direccion = models.CharField(max_length=200, blank=True, null=True)
    observacion = models.TextField(blank=True, null=True)

    def __str__(self):
        return self.nombres + " " + self.apellidos + " | cc. " + self.ide


class Empleado(models.Model):
    ide = models.CharField( max_length=50)
    tipo = models.ForeignKey( 'TipoIdentificacion',on_delete=models.CASCADE, blank=True, null=True)
    nombres = models.CharField(max_length=100, blank=True, null=True)
    apellidos = models.CharField(max_length=100, blank=True, null=True)
    cod_cargo = models.ForeignKey( 'Cargo', on_delete=models.CASCADE, blank=True, null=True)
    fecha_nac = models.CharField(max_length=100,blank=True, null=True)
    lugar_nac = models.CharField(max_length=300, blank=True, null=True)
    direccion = models.CharField(max_length=200, blank=True, null=True)
    contacto = models.CharField(max_length=100, blank=True, null=True)
    fotografia = models.ImageField(upload_to = 'ImgEmpleados/', blank=True, null=True,)
    email = models.CharField(max_length=100, blank=True, null=True)
    observacion = models.TextField(blank=True, null=True)
    eps = models.ForeignKey( 'SeguridadSocial',on_delete=models.CASCADE,blank=True, null=True, related_name='fk_eps' )
    arl = models.ForeignKey( 'SeguridadSocial',on_delete=models.CASCADE,blank=True, null=True, related_name='fk_ark')
    pension = models.ForeignKey( 'SeguridadSocial',on_delete=models.CASCADE,blank=True, null=True, related_name='fk_pension' )
    cesantias = models.ForeignKey( 'SeguridadSocial',on_delete=models.CASCADE,blank=True, null=True, related_name='fk_cesantias')

    def __str__(self):
        return self.id


class Estado(models.Model):
    ide = models.IntegerField()
    detalle = models.CharField(max_length=50, blank=True, null=True)
       
    def __str__(self):
        return self.id


class Estante(models.Model):
    ide = models.IntegerField()
    nombre = models.CharField(max_length=200, blank=True, null=True)
        
    def __str__(self):
        return self.nombre

   

class Familia(models.Model):
    ide = models.IntegerField()
    nombre = models.CharField(max_length=200, blank=True, null=True)
        
    def __str__(self):
        return self.nombre


class Producto(models.Model):
    referencias = models.CharField( max_length=50)
    descripcion = models.CharField(max_length=200, blank=True, null=True)
    fotografia = models.ImageField(upload_to = 'ImgProductos/', blank=True, null=True)
    costo = models.FloatField(blank=True, null=True)
    precio_unit = models.FloatField(blank=True, null=True)
    stock_minimo = models.IntegerField(blank=True, null=True)
    familia = models.ForeignKey( Familia,on_delete=models.CASCADE,   blank=True, null=True)
    estante = models.ForeignKey( Estante, on_delete=models.CASCADE, blank=True, null=True)
    observacion = models.TextField(blank=True, null=True)
    cantidad_inventario = models.IntegerField()
    
    def __str__(self):
        return self.referencias

    def stock_alert(self):
        diferencia = self.cantidad_inventario - self.stock_minimo
        if (diferencia >= 0):
            return "success"
        elif(diferencia < 0):
            return "danger"
    
    def stock_buy(self):
        diferencia = self.cantidad_inventario - self.stock_minimo
        if(diferencia < 0):
            return True
    
    def cantidad_comprar(self):
        diferencia = self.cantidad_inventario - self.stock_minimo
        comprar = diferencia*-1
        return comprar

        


class OrdenItem(models.Model):
    producto = models.OneToOneField(Producto, on_delete=models.SET_NULL, null=True)

    def __str__(self):
        return self.producto.descripcion

class Orden(models.Model):
    codigo= models.CharField(max_length=15)
    cliente = models.ForeignKey(Cliente, on_delete=models.SET_NULL, null=True)
    is_ordered = models.BooleanField(default=False)
    items = models.ManyToManyField(OrdenItem)
    date_ordered = models.DateTimeField(auto_now=True)

    def get_cart_items(self):
        return self.items.all()
    
    def get_cart_total(self):
        return sum([item.producto.precio_unit for item in self.items.all()])

    def __str__(self):
        return '{0} - {1}'.format(self.owner, self.ref_code)






class Proveedor(models.Model):
    ide = models.CharField( max_length=50)
    tipo = models.ForeignKey( 'TipoIdentificacion',on_delete=models.CASCADE, blank=True, null=True)
    razon_social = models.CharField(max_length=500, blank=True, null=True)
    contacto = models.CharField(max_length=100, blank=True, null=True)
    email = models.CharField(max_length=100, blank=True, null=True)
    tipo_persona = models.ForeignKey( 'TipoPersona', on_delete=models.CASCADE,  blank=True, null=True)
    direccion = models.CharField(max_length=200, blank=True, null=True)
    observacion = models.TextField(blank=True, null=True)

    def __str__(self):
        return self.id



class SeguridadSocial(models.Model):
    ide = models.IntegerField()
    nombre = models.CharField(max_length=200, blank=True, null=True)
    tipo = models.ForeignKey( 'TipoSeguridad',on_delete=models.CASCADE, blank=True, null=True)

    def __str__(self):
        return self.nombre


class TipoIdentificacion(models.Model):
    ide = models.CharField( max_length=4)
    nombre = models.CharField(max_length=100, blank=True, null=True)

    def __str__(self):
        return self.nombre


class TipoPersona(models.Model):
    ide = models.CharField( max_length=1)
    nombre = models.CharField(max_length=50, blank=True, null=True)

    def __str__(self):
        return self.nombre


class TipoSeguridad(models.Model):
    ide = models.IntegerField()
    nombre = models.CharField(max_length=50, blank=True, null=True)

    def __str__(self):
        return self.nombre

@receiver(post_delete, sender=Empleado)
def fotografia_delete(sender, instance, **kwargs):
    """ Borra los ficheros de las fotos que se eliminan. """
    instance.fotografia.delete(False)