from django.urls import path
from .views import ApiListaProductos

urlpatterns = [
    path('ListaProductos/', ApiListaProductos.as_view(), name="ApiListaProductos"),
]