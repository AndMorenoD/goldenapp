from django.shortcuts import render, redirect, get_object_or_404
from AppFerreteria.models import *
from AppFerreteria import forms
from django.contrib.auth.mixins import LoginRequiredMixin
from django.contrib.auth.decorators import login_required
from django.urls import reverse_lazy, reverse 
from django.views.generic.edit import UpdateView, CreateView, DeleteView
from django.views.generic import ListView, TemplateView
from django.core import serializers
from django.http import HttpResponse

################CRUD EMPLEADO
@login_required
def listaEmpleados(request):
    lista = Empleado.objects.all()
    lista_empleados = {'empleados':lista}
    return render(request, 'Empleado/empleadosLista.html', lista_empleados)

class EmpleadoCreate(CreateView, LoginRequiredMixin):
    model = Empleado
    form_class = forms.EmpleadoForm
    template_name = 'Empleado/empleadoCreate.html'
    success_url = reverse_lazy('lista_empleados')

class EmpleadoUpdate(UpdateView, LoginRequiredMixin):
    model = Empleado
    form_class = forms.EmpleadoForm
    template_name = 'Empleado/empleadoUpdate.html'
    success_url = reverse_lazy('lista_empleados')

class EmpleadoDelete(DeleteView, LoginRequiredMixin):
    model = Empleado
    template_name = 'Empleado/empleado_confirm_delete.html'
    success_url = reverse_lazy('lista_empleados')

#############CRUD CLIENTES
@login_required
def listaClientes(request):
    lista = Cliente.objects.all()
    lista_clientes = {'clientes':lista}
    return render(request, 'Cliente/clientesLista.html', lista_clientes)

class ClienteCreate(CreateView, LoginRequiredMixin):
    model = Cliente
    form_class = forms.ClienteForm
    template_name = 'Cliente/clienteCreate.html'
    success_url = reverse_lazy('lista_clientes')

class ClienteUpdate(UpdateView, LoginRequiredMixin):
    model = Cliente
    form_class = forms.ClienteForm
    template_name = 'Cliente/clienteUpdate.html'
    success_url = reverse_lazy('lista_clientes')

class ClienteDelete(DeleteView, LoginRequiredMixin):
    model = Cliente
    template_name = 'Cliente/cliente_confirm_delete.html'
    success_url = reverse_lazy('lista_clientes')

############CRUD PRODUCTO
@login_required
def listaProductos(request):
    lista = Producto.objects.all()
    lista_productos = {'productos':lista}
    return render(request, 'Producto/productosLista.html', lista_productos)



class ProductoCreate(CreateView, LoginRequiredMixin):
    model = Producto
    form_class = forms.ProductoForm
    template_name = 'Producto/productoCreate.html'
    success_url = reverse_lazy('lista_productos')

class ProductoUpdate(UpdateView, LoginRequiredMixin):
    model = Producto
    form_class = forms.ProductoForm
    template_name = 'Producto/productoUpdate.html'
    success_url = reverse_lazy('lista_productos')

class ProductoDelete(DeleteView, LoginRequiredMixin):
    model = Producto
    template_name = 'Producto/producto_confirm_delete.html'
    success_url = reverse_lazy('lista_productos')

############CRUD PROVEEDORES
@login_required
def listaProveedores(request):
    lista = Proveedor.objects.all()
    lista_proveedores = {'proveedores':lista}
    return render(request, 'Proveedor/ProveedoresLista.html', lista_proveedores)

class ProveedorCreate(CreateView, LoginRequiredMixin):
    model = Proveedor
    form_class = forms.ProveedorForm
    template_name = 'Proveedor/ProveedorCreate.html'
    success_url = reverse_lazy('lista_proveedores')

class ProveedorUpdate(UpdateView, LoginRequiredMixin):
    model = Proveedor
    form_class = forms.ProveedorForm
    template_name = 'Proveedor/ProveedorUpdate.html'
    success_url = reverse_lazy('lista_proveedores')

class ProveedorDelete(DeleteView, LoginRequiredMixin):
    model = Proveedor
    template_name = 'Proveedor/Proveedor_confirm_delete.html'
    success_url = reverse_lazy('lista_proveedores')

###### VENTA #########
class CrearVenta(CreateView):
    model = Orden
    form_class = forms.VentaForm
    template_name = 'Venta/Venta.html'
    success_url = reverse_lazy('lista_proveedores')

class CrearOrdenItem(CreateView):
    model = OrdenItem
    form_class = forms.OrdenItemForm
    template_name = 'Venta/OrdenItem.html'
    success_url = reverse_lazy('crear_venta')

class ListaAjaxProductos(TemplateView):

    def get(self, request, *args, **kwards):
        productos = Producto.objects.all()
        data = serializers.serialize('json', productos, fields=('referencias','descripcion','precio_unit', 'stock_minimo','estante'))
        return HttpResponse(data,content_type="application/json")
        


    



#                              MINI CRUDS                                         #

#CARGO CRUD
@login_required
def listaCargo(request):
    lista = Cargo.objects.all()
    lista_cargo = {'cargo':lista}
    return render(request, 'MiniCruds/Cargo/cargoLista.html', lista_cargo)

class CargoCreate(CreateView, LoginRequiredMixin):
    model = Cargo
    form_class = forms.CargoForm
    template_name = 'MiniCruds/Cargo/CargoCreate.html'
    success_url = reverse_lazy('lista_cargos')

class CargoUpdate(UpdateView, LoginRequiredMixin):
    model = Cargo
    form_class = forms.CargoForm
    template_name = 'MiniCruds/Cargo/CargoUpdate.html'
    success_url = reverse_lazy('lista_cargos')

class CargoDelete(DeleteView, LoginRequiredMixin):
    model = Cargo
    template_name = 'MiniCruds/Cargo/Cargo_confirm_delete.html'
    success_url = reverse_lazy('lista_cargos')

#ESTADO CRUD
@login_required
def listaEstado(request):
    lista = Estado.objects.all()
    lista_estado = {'Estado':lista}
    return render(request, 'MiniCruds/Estado/EstadoLista.html', lista_estado)

class EstadoCreate(CreateView, LoginRequiredMixin):
    model = Estado
    form_class = forms.EstadoForm
    template_name = 'MiniCruds/Estado/EstadoCreate.html'
    success_url = reverse_lazy('lista_Estados')

class EstadoUpdate(UpdateView, LoginRequiredMixin):
    model = Estado
    form_class = forms.EstadoForm
    template_name = 'MiniCruds/Estado/EstadoUpdate.html'
    success_url = reverse_lazy('lista_Estados')

class EstadoDelete(DeleteView, LoginRequiredMixin):
    model = Estado
    template_name = 'MiniCruds/Estado/Estado_confirm_delete.html'
    success_url = reverse_lazy('lista_Estados')

#ESTADO ESTANTES
@login_required
def listaEstante(request):
    lista = Estante.objects.all()
    lista_Estante = {'Estante':lista}
    return render(request, 'MiniCruds/Estante/EstanteLista.html', lista_Estante)

class EstanteCreate(CreateView, LoginRequiredMixin):
    model = Estante
    form_class = forms.EstanteForm
    template_name = 'MiniCruds/Estante/EstanteCreate.html'
    success_url = reverse_lazy('lista_Estantes')

class EstanteUpdate(UpdateView, LoginRequiredMixin):
    model = Estante
    form_class = forms.EstanteForm
    template_name = 'MiniCruds/Estante/EstanteUpdate.html'
    success_url = reverse_lazy('lista_Estantes')

class EstanteDelete(DeleteView, LoginRequiredMixin):
    model = Estante
    template_name = 'MiniCruds/Estante/Estante_confirm_delete.html'
    success_url = reverse_lazy('lista_Estantes')

#ESTADO FAMILIA
@login_required
def listaFamilia(request):
    lista = Familia.objects.all()
    lista_Familia = {'Familia':lista}
    return render(request, 'MiniCruds/Familia/FamiliaLista.html', lista_Familia)

class FamiliaCreate(CreateView, LoginRequiredMixin):
    model = Familia
    form_class = forms.FamiliaForm
    template_name = 'MiniCruds/Familia/FamiliaCreate.html'
    success_url = reverse_lazy('lista_Familias')

class FamiliaUpdate(UpdateView, LoginRequiredMixin):
    model = Familia
    form_class = forms.FamiliaForm
    template_name = 'MiniCruds/Familia/FamiliaUpdate.html'
    success_url = reverse_lazy('lista_Familias')

class FamiliaDelete(DeleteView, LoginRequiredMixin):
    model = Familia
    template_name = 'MiniCruds/Familia/Familia_confirm_delete.html'
    success_url = reverse_lazy('lista_Familias')

#ESTADO SEGURIDAD
@login_required
def listaSeguridadSocial(request):
    lista = SeguridadSocial.objects.all()
    lista_SeguridadSocial = {'SeguridadSocial':lista}
    return render(request, 'MiniCruds/SeguridadSocial/SeguridadSocialLista.html', lista_SeguridadSocial)

class SeguridadSocialCreate(CreateView, LoginRequiredMixin):
    model = SeguridadSocial
    form_class = forms.SeguridadSocialForm
    template_name = 'MiniCruds/SeguridadSocial/SeguridadSocialCreate.html'
    success_url = reverse_lazy('lista_SeguridadSocial')

class SeguridadSocialUpdate(UpdateView, LoginRequiredMixin):
    model = SeguridadSocial
    form_class = forms.SeguridadSocialForm
    template_name = 'MiniCruds/SeguridadSocial/SeguridadSocialUpdate.html'
    success_url = reverse_lazy('lista_SeguridadSocial')

class SeguridadSocialDelete(DeleteView, LoginRequiredMixin):
    model = SeguridadSocial
    template_name = 'MiniCruds/SeguridadSocial/SeguridadSocial_confirm_delete.html'
    success_url = reverse_lazy('lista_SeguridadSocial')

#ESTADO TIPO DE SEGURIDAD
@login_required
def listaTipoSeguridad(request):
    lista = TipoSeguridad.objects.all()
    lista_TipoSeguridad = {'TipoSeguridad':lista}
    return render(request, 'MiniCruds/TipoSeguridad/TipoSeguridadLista.html', lista_TipoSeguridad)

class TipoSeguridadCreate(CreateView, LoginRequiredMixin):
    model = TipoSeguridad
    form_class = forms.TipoSeguridadForm
    template_name = 'MiniCruds/TipoSeguridad/TipoSeguridadCreate.html'
    success_url = reverse_lazy('lista_TipoSeguridad')

class TipoSeguridadUpdate(UpdateView, LoginRequiredMixin):
    model = TipoSeguridad
    form_class = forms.TipoSeguridadForm
    template_name = 'MiniCruds/TipoSeguridad/TipoSeguridadUpdate.html'
    success_url = reverse_lazy('lista_TipoSeguridad')

class TipoSeguridadDelete(DeleteView, LoginRequiredMixin):
    model = TipoSeguridad
    template_name = 'MiniCruds/TipoSeguridad/TipoSeguridad_confirm_delete.html'
    success_url = reverse_lazy('lista_TipoSeguridad')

#TIPO DE IDENTIFICACION
@login_required
def listaTipoIdentificacion(request):
    lista = TipoIdentificacion.objects.all()
    lista_TipoIdentificacion = {'TipoIdentificacion':lista}
    return render(request, 'MiniCruds/TipoIdentificacion/TipoIdentificacionLista.html', lista_TipoIdentificacion)

class TipoIdentificacionCreate(CreateView, LoginRequiredMixin):
    model = TipoIdentificacion
    form_class = forms.TipoIdentificacionForm
    template_name = 'MiniCruds/TipoIdentificacion/TipoIdentificacionCreate.html'
    success_url = reverse_lazy('lista_TipoIdentificacion')

class TipoIdentificacionUpdate(UpdateView, LoginRequiredMixin):
    model = TipoIdentificacion
    form_class = forms.TipoIdentificacionForm
    template_name = 'MiniCruds/TipoIdentificacion/TipoIdentificacionUpdate.html'
    success_url = reverse_lazy('lista_TipoIdentificacion')

class TipoIdentificacionDelete(DeleteView, LoginRequiredMixin):
    model = TipoIdentificacion
    template_name = 'MiniCruds/TipoIdentificacion/TipoIdentificacion_confirm_delete.html'
    success_url = reverse_lazy('lista_TipoIdentificacion')

#TIPO DE PERSONA
@login_required
def listaTipoPersona(request):
    lista = TipoPersona.objects.all()
    lista_TipoPersona = {'TipoPersona':lista}
    return render(request, 'MiniCruds/TipoPersona/TipoPersonaLista.html', lista_TipoPersona)

class TipoPersonaCreate(CreateView, LoginRequiredMixin):
    model = TipoPersona
    form_class = forms.TipoPersonaForm
    template_name = 'MiniCruds/TipoPersona/TipoPersonaCreate.html'
    success_url = reverse_lazy('lista_TipoPersona')

class TipoPersonaUpdate(UpdateView, LoginRequiredMixin):
    model = TipoPersona
    form_class = forms.TipoPersonaForm
    template_name = 'MiniCruds/TipoPersona/TipoPersonaUpdate.html'
    success_url = reverse_lazy('lista_TipoPersona')

class TipoPersonaDelete(DeleteView, LoginRequiredMixin):
    model = TipoPersona
    template_name = 'MiniCruds/TipoPersona/TipoPersona_confirm_delete.html'
    success_url = reverse_lazy('lista_TipoPersona')