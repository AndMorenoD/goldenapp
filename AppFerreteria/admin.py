from django.contrib import admin
from .models import *
# Register your models here.
admin.site.register(Cargo)
admin.site.register(Cliente)
admin.site.register(Estado)
admin.site.register(Estante)
admin.site.register(Familia)
admin.site.register(Producto)
admin.site.register(Proveedor)
admin.site.register(SeguridadSocial)
admin.site.register(TipoIdentificacion)
admin.site.register(TipoPersona)
admin.site.register(TipoSeguridad)

