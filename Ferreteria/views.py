from django.shortcuts import render

def bienvenida(request):
    return render(request, 'bienvenida.html')

def base(request):
    return render(request, 'base.html')

def gestionSoftware(request):
    return render(request, 'gestionSoftware.html')